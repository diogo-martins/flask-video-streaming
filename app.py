from importlib import import_module
import os
from flask import Flask, render_template, Response
# import camera driver
#from camera_opencv import Camera
from camera import Camera

import cv2
from object_detection import ObjectDetection

app = Flask(__name__)
objectDetector = ObjectDetection('models/OceanusNet_quantized_50k', 0.5, False)


@app.route('/')
def index():
    """Real time object detection."""
    return render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001', threaded=True)
   # TODO: cleanup inference code, frame counts, write to db