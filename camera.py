######################### OpenCV Camera #####################################
# 
# Class that simulates a camera by loading an image from a file and returning it 
# 
#############################################################################

import time
import os
from base_camera import BaseCamera


PROGRAM_PATH = os.path.split(__file__)[0] + "/"
if PROGRAM_PATH == "/":
    PROGRAM_PATH = ""


class Camera(BaseCamera):
    """An emulated camera implementation that streams a repeated sequence of
    files 1.jpg, 2.jpg and 3.jpg at a rate of one frame per second."""
    
    @staticmethod
    def frames():
        global PROGRAM_PATH
        while True:
            imgs = [open(f + '.jpg', 'rb').read() for f in [PROGRAM_PATH + 'frame']] # Opens the image frame.jpg and stores it in an array
            time.sleep(.5)
            yield imgs[0] # returns the image previously opened