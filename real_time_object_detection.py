######## Webcam Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 10/27/19
# Description: 
# This program uses a TensorFlow Lite model to perform object detection on a live webcam
# feed. It draws boxes and scores around the objects of interest in each frame from the
# webcam. To improve FPS, the webcam object runs in a separate thread from the main program.
# This script will work with either a Picamera or regular USB webcam.
#
# This code is based off the TensorFlow Lite image classification example at:
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/python/label_image.py
#
# I added my own method of drawing boxes and labels using OpenCV.

# Import packages
import os
import argparse
import cv2
import numpy as np
import sys
import time
from threading import Thread
import importlib.util
import requests
import json
import datetime
from PIL import ImageFont, ImageDraw, Image

from object_detection import ObjectDetection

##############################
# GLOBALS
##############################

# Get this program's directory
PROGRAM_PATH = os.path.split(__file__)[0] + "/"
if PROGRAM_PATH == "/":
    PROGRAM_PATH = ""

API_URL = 'http://localhost:5000'
frames_counter = 0 # this variable is used to count frames
detection_interval = 10 # minimum number of frames between detections saved, to avoid multiple images that are frames following each other


##############################
# Functions
##############################

# makes a post to the API that writes the data in the db
def write_to_db(endpoint, data_json):
    url = API_URL + '/' + endpoint
    
    try:
        response = requests.post(url, json=data_json)
    except Exception as err:
        print(err)
        response = False
    
    return response


# create image folders if they do not exist
if not os.path.exists(PROGRAM_PATH + '/detections/'):
    os.makedirs(PROGRAM_PATH + '/detections')

if not os.path.exists(PROGRAM_PATH + '/detections/unreviewed'):
    os.makedirs(PROGRAM_PATH + '/detections/unreviewed')

if not os.path.exists(PROGRAM_PATH   + '/detections/reviewed'):
    os.makedirs(PROGRAM_PATH + '/detections/reviewed')

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in',
                    default='models/OceanusNet_quantized_50k')
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects',
                    default=0.5)
parser.add_argument('--resolution', help='Desired webcam resolution in WxH. If the webcam does not support the resolution entered, errors may occur.',
                    default='1280x720')
parser.add_argument('--edgetpu', help='Use Coral Edge TPU Accelerator to speed up detection',
                    action='store_true')

args = parser.parse_args()

MODEL_DIR = args.modeldir
min_conf_threshold = float(args.threshold)
use_TPU = args.edgetpu
resW, resH = args.resolution.split('x')
imW, imH = int(resW), int(resH)


objectDetector = ObjectDetection(MODEL_DIR, min_conf_threshold, use_TPU)


# Define VideoStream class to handle streaming of video from webcam in separate processing thread
# Source - Adrian Rosebrock, PyImageSearch: https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/
class VideoStream:
    """Camera object that controls video streaming from the Picamera"""
    def __init__(self,resolution=(imW,imH),framerate=30):
        # Initialize the PiCamera and the camera image stream
        self.stream = cv2.VideoCapture(0)
        ret = self.stream.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        ret = self.stream.set(3,resolution[0])
        ret = self.stream.set(4,resolution[1])
            
        # Read first frame from the stream
        (self.grabbed, self.frame) = self.stream.read()

	# Variable to control when the camera is stopped
        self.stopped = False

    def start(self):
	# Start the thread that reads frames from the video stream
        Thread(target=self.update,args=()).start()
        return self

    def update(self):
        # Keep looping indefinitely until the thread is stopped
        while True:
            # If the camera is stopped, stop the thread
            if self.stopped:
                # Close camera resources
                self.stream.release()
                return

            # Otherwise, grab the next frame from the stream
            (self.grabbed, self.frame) = self.stream.read()

    def read(self):
	# Return the most recent frame
        return self.frame

    def stop(self):
	# Indicate that the camera and thread should be stopped
        self.stopped = True

# Initialize frame rate calculation
frame_rate_calc = 1
freq = cv2.getTickFrequency()

# Initialize video stream
videostream = VideoStream(resolution=(imW,imH),framerate=30).start()
time.sleep(1)

#for frame1 in camera.capture_continuous(rawCapture, format="bgr",use_video_port=True):
while True:
    frames_counter += 1
    timestamp = datetime.datetime.now()
    
    # Start timer (for calculating frame rate)
    t1 = cv2.getTickCount()

    # Grab frame from video stream
    frame = videostream.read()
    
    # Save a copy of the original frame to write to a file if a detection occurs
    originalFrame = frame.copy()

    inferredFrame, detectionJSON = objectDetector.imageInference(frame)
    objectsJSON = detectionJSON.pop('objects', None)

    if len(objectsJSON) > 0:
        object_detected = True
    else:
        object_detected = False
    
    detection_response = False
    if object_detected and frames_counter > detection_interval:
        # Save the image to detections directory
        date_time_obj = timestamp
        date_time_str = str(date_time_obj.time()).replace(':', 'h', 1).replace(':', 'm').replace('.','s')
        image_path = PROGRAM_PATH + 'detections/unreviewed/' + str(date_time_obj.date()) + '_' + date_time_str + ".jpg"
        
        cv2.imwrite(image_path, originalFrame)
        print('Detection saved at: ' + image_path)

        detectionJSON['image_path'] = image_path
        
        detection_response = write_to_db("detection", detectionJSON)
        
        if detection_response:
            inserted_detection_id = detection_response.json()['inserted_id']

            # save image with bboxes to a file
            image_path = PROGRAM_PATH + '/detections/unreviewed/' + str(date_time_obj.date()) + '_' + date_time_str + "_BBOXES.jpg"
            cv2.imwrite(image_path, inferredFrame)

            # write object data to db            
            for key in objectsJSON:
                objectsJSON[key]['detection_id'] = inserted_detection_id
                object_response = write_to_db("object", objectsJSON[key])

                if object_response:
                    print("Object: " + str(object_response.json()))
                    #inserted_object_id = object_response.json()['inserted_id']
                else:
                    print("Cannot connect to the API(object).")
        else:
            print("Cannot connect to the API(detection).")
            
    # draw fps counter after saving the image with the bboxes
    cv2.rectangle(inferredFrame, (20,20), (97, 45), (0,0,0), cv2.FILLED) # FPS background box
    cv2.rectangle(inferredFrame, (20,20), (97, 45), (255,255,255), 1) # border for FPS background box 
    inferredFrame = objectDetector.draw_label(inferredFrame, 'FPS: {0:.2f}'.format(frame_rate_calc), 25, 23, (255,255,255,255), 15)
    
    # resets interval if a detection was saved
    if object_detected and detection_response != False:
        frames_counter = 0

    # Save the inferredFrame in a file to be streamed
    cv2.imwrite(PROGRAM_PATH + '/frame.jpg', inferredFrame)

    # Calculate framerate
    t2 = cv2.getTickCount()
    time1 = (t2-t1)/freq
    frame_rate_calc= 1/time1

# Clean up
cv2.destroyAllWindows()
videostream.stop()
